SHELL := /bin/bash

run:
	docker-compose up -d

migrate:
	docker-compose exec app php /var/www/app/bin/console migrations:migrate -n

fetch:
	docker-compose exec app php /var/www/app/bin/console fetch:trailers --limit=10
