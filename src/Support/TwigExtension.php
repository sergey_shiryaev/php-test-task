<?php

declare(strict_types=1);

namespace App\Support;

use Slim\Interfaces\RouteCollectorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TwigExtension
 * @see AbstractExtension
 */
class TwigExtension extends  AbstractExtension
{
    /**
     * @param RouteCollectorInterface $routeCollector
     */
    public function __construct(RouteCollectorInterface $routeCollector)
    {
        $this->routeCollector = $routeCollector;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('url_for', [$this, 'urlFor'])
        ];
    }

    /**
     * @param string $routeName
     * @param array $routeArgs
     * @param array $queryParams
     *
     * @return string
     */
    public function urlFor(string $routeName, array $routeArgs = [], array $queryParams = []): string
    {
        return $this->routeCollector->getRouteParser()->urlFor($routeName, $routeArgs, $queryParams);

    }
}